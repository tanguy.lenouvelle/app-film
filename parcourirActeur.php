<?php include 'functions.php' ?>
<?php include 'functionsActeur.php' ?>
<?php include 'header_utilisateur.php' ?>
<title>Acteur</title>

<form action="ficheActeur.php" method="post">
	<div class="row">
		<div class="col s4">
	<label for="acteur"><h5> Choisissez un acteur parmis la liste</h5></label><br>
</div>
</div>
	
    <div class="row">
    	<div class="col s4">
    <select class="browser-default" name="acteur_id">
		<?php foreach($tableActeur as $value):?>
		<option value="<?php echo $value['acteur_id'];?>">
			<?php echo $value['acteur_nom'];?>
		</option>
		<?php endforeach;?>
	</select><br>	
	</div>
</div>
	
	<div class="row">
	<div class="col s3 offset-s1 ">
  	<button class="waves-effect waves-light btn light-blue darken-3" name="form_submit" value="1" type="submit">Confirmer</button>
  </div>
</div>

</form>

<div class="row">
	<div class="col s3 offset-s1 ">
<form action="touslesacteurs.php">
  <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Voir tous les acteurs</button>
</form>
<br>

<form action="index_utilisateur.php">
  <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Retour à l'accueil</button>
</form>
</div>
</div>
<?php include 'footer.php' ?>