<?php include 'header_utilisateur.php' ?>
<?php
    
session_start();

if(!isset($_SESSION['pseudo'])){
    header("Refresh: 5; url=connexion.php");
    echo "Vous devez vous connecter pour accéder à l'espace membre.<br><br><i>Redirection en cours, vers la page de connexion...</i>";
    exit(0);
}
$Pseudo=$_SESSION['pseudo'];

$mysqli=mysqli_connect('localhost','root','','bdd film');

if(!$mysqli) {
    echo "Erreur connexion BDD";
    
    exit(0);
}

$req=mysqli_query($mysqli,"SELECT * FROM membres WHERE pseudo='$Pseudo'");
$info=mysqli_fetch_assoc($req);
?><!DOCTYPE HTML>
<html>
    <head>

        <title>espace membre</title>
    </head>
    <body>
     <div class="row">
        <div class="col s4">
        <h3>Espace membre</h3><br>
    </div>
</div>
        <div class="row">
    <div class="col s3 offset-s1 ">
            <form method="post" action="espace-membre.php?modifier">
            <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="" value="modifier le compte">
            </form>
        <br>
        
            <form method="post" action="espace-membre.php?supprimer">
            <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="" value="Supprimer le compte">
            </form>
        <br>
        
            <form method="post" action="deconnexion.php">
            <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="" value="Se deconnecter">
            </form>

        <br>

            <form method="post" action="index_utilisateur.php">
            <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="" value="Retour à l'accueil"><br>
            </form>
        </div>
    </div>

        
        <?php

            if(isset($_GET['supprimer'])){
            if($_GET['supprimer']!="ok"){
                echo "<div class='row'>";
                echo "<div class='col s5'>";
                echo "<p>Êtes-vous sûr de vouloir supprimer votre compte définitivement?</p>
                <br>

                <a href='espace-membre.php?supprimer=ok' style='color:red'>OUI</a> - <a href='espace-membre.php' style='color:green'>NON</a>";
                echo "</div>";
                echo "</div>";
            } else {
                
                if(mysqli_query($mysqli,"DELETE FROM membres WHERE pseudo='$Pseudo'")){
                    echo "Votre compte vient d'être supprimé définitivement.";
                    unset($_SESSION['pseudo']);
                } else {
                    echo "Une erreur est survenue, merci de réessayer ou contactez-nous si le problème persiste.";
                    
                }
            }
        }
        
        if(isset($_GET['modifier'])){
            ?>
            <div class="row">
    <div class="col s4 offset-s1 ">
            <h4>Modification du compte</h4><br>
            Choisissez une option: 
            <p>
            </div>
        </div>
                  <div class="row">
    <div class="col s3 offset-s1 ">
                <form method="post" action="espace-membre.php?modifier=mail">
                <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="" value="Modifier l'adresse mail">
                </form>

                <br>
                
                <form method="post" action="espace-membre.php?modifier=mdp">
                <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="" value="Modifier le mot de passe">
                </form>
</div>
</div>
            </p>
            
            <?php
            if($_GET['modifier']=="mail"){
                echo "<div class='row'>";
                echo "<div class='col s5'>";
                echo "<p>Renseignez le formulaire ci-dessous pour modifier vos informations:</p>";
                echo "</div>";
                echo "</div>";
                if(isset($_POST['valider'])){
                    if(!isset($_POST['mail'])){
                        echo "Le champ mail n'est pas reconnu.";
                    } else {
                        if(!preg_match("#^[a-z0-9_-]+((\.[a-z0-9_-]+){1,})?@[a-z0-9_-]+((\.[a-z0-9_-]+){1,})?\.[a-z]{2,30}$#i",$_POST['mail'])){
                            
                            echo "L'adresse mail est incorrecte.";
                            
                        } else {
                            
                            if(mysqli_query($mysqli,"UPDATE membres SET mail='".htmlentities($_POST['mail'],ENT_QUOTES,"UTF-8")."' WHERE pseudo='$Pseudo'")){
                                echo "Adresse mail {$_POST['mail']} modifiée avec succès!";
                                $TraitementFini=true;
                            } else {
                                echo "Une erreur est survenue, merci de réessayer ou contactez-nous si le problème persiste.";
                                
                            }
                        }
                    }
                }
                if(!isset($TraitementFini)){
                    ?>
                    
                      <div class="row">
                            <div class="col s3 offset-s1 ">
                    <form method="post" action="espace-membre.php?modifier=mail">
                        <input type="email" name="mail" value="<?php echo $info['mail']; ?>" required>

                        <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="valider" value="Valider la modification">
                    </form>
                </div>
            </div>
                    <?php
                }
            } elseif($_GET['modifier']=="mdp"){
                echo "<div class='row'>";
                echo "<div class='col s5'>";
                echo "<p>Renseignez le formulaire ci-dessous pour modifier vos informations:</p>";
                echo "</div>";
                echo "</div>";
                if(isset($_POST['valider'])){
                    
                    if(!isset($_POST['nouveau_mdp'],$_POST['confirmer_mdp'],$_POST['mdp'])){
                        echo "Un des champs n'est pas reconnu.";
                    } else {
                        if($_POST['nouveau_mdp']!=$_POST['confirmer_mdp']){
                            echo "Les mots de passe ne correspondent pas.";
                        } else {
                            $Mdp=md5($_POST['mdp']);
                            $NouveauMdp=md5($_POST['nouveau_mdp']);
                            $req=mysqli_query($mysqli,"SELECT * FROM membres WHERE pseudo='$Pseudo' AND mdp='$Mdp'");
                            
                            if(mysqli_num_rows($req)!=1){
                                echo "Mot de passe actuel incorrect.";
                            } else {
                                
                                if(mysqli_query($mysqli,"UPDATE membres SET mdp='$NouveauMdp' WHERE pseudo='$Pseudo'")){
                                    echo "Mot de passe modifié avec succès!";
                                    $TraitementFini=true;
                                } else {
                                    echo "Une erreur est survenue, merci de réessayer ou contactez-nous si le problème persiste.";
                                    
                                }
                            }
                        }
                    }
                }
                if(!isset($TraitementFini)){
                    ?>
                    <br>
                     <div class="row">
                            <div class="col s3 offset-s1 ">
                    <form method="post" action="espace-membre.php?modifier=mdp">
                        <input type="password" name="nouveau_mdp" placeholder="Nouveau mot de passe..." required>
                        <input type="password" name="confirmer_mdp" placeholder="Confirmer nouveau passe..." required>
                        <input type="password" name="mdp" placeholder="Votre mot de passe actuel..." required>
                        <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="valider" value="Valider la modification">
                    </form>
                </div>
            </div>
                    <?php
                }
            }
        }
        ?>
    </body>
</html>

<?php include 'footer.php' ?>