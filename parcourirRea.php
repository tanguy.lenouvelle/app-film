<?php include 'functions.php' ?>
<?php include 'header_utilisateur.php' ?>
<title>Réalisateur</title>
<form action="ficheRealisateur.php" method="post">
	<div class="row">
		<div class="col s5">
	<label for="realisateur"><h5> Choisissez un réalisateur parmis la liste</h5></label><br>
</div>
</div>
	
        <div class="row">
    	<div class="col s4">
    <select class="browser-default" name="realisateur_id">
		<?php foreach($tableRealisateur as $value):?>
		<option value="<?php echo $value['realisateur_id'];?>">
			<?php echo $value['realisateur_nom'];?>
		</option>
		<?php endforeach;?>
	</select>
	</div>
</div>
<div class="row">
	<div class="col s3 offset-s1 ">	
	<br>
  	<button class="waves-effect waves-light btn light-blue darken-3" name="form_submit" value="1" type="submit">Confirmer</button>
</div>
</div>
</form>


<div class="row">
	<div class="col s3 offset-s1 ">
<form action="touslesrealisateurs.php">
  <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Voir tous les realisateurs</button>
</form>
<br>

<form action="index_utilisateur.php">
  <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Retour à l'accueil</button>
</form>
</div>
</div>
<?php include 'footer.php' ?>