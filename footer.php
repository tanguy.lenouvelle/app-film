</main>
	    <footer class="page-footer blue-grey lighten-1">
        
          <div class="container">
            
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Informations</h5>
                <p class="grey-text text-lighten-4">Consultez la fiche technique du film que vous souhaitez, mais également l'acteur ou le réalisateur dont vous souhaitez obtenir des informations.</p>
              </div>
              <div class="col l4 offset-l2 s12" >
                <h5 class="white-text">Liens</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="espace-membre.php">Espace Membre</a></li>
                  <li><a class="grey-text text-lighten-3" href="deconnexion.php">Deconnexion</a></li>
                  
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2020 APPLI-Film
            
            </div>
          </div>
        </footer>
       
   <script src = "script/front/script.js"></script>
      </body>
      </html>