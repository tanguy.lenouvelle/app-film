<?php include 'header_bis.php' ?> 
<?php
    
session_start();
?><!DOCTYPE HTML>
<html>
    <head>
        <title>Connexion</title>
    </head>
    <body>
        <div class="center">
        <h1>Connexion</h1>
                
     

        <?php
        
        if(isset($_SESSION['pseudo'])){
            echo "Vous êtes déjà connecté, vous pouvez accéder à l'accueil du site en <a href='index_bis.php'>cliquant ici</a>.";
        } else {
            
            if(isset($_POST['valider'])){
                
                if(!isset($_POST['pseudo'],$_POST['mdp'])){
                    echo "Un des champs n'est pas reconnu.";
                } else {
                    
                    $mysqli=mysqli_connect('localhost','root','','bdd film');

                    if(!$mysqli) {
                        echo "Erreur connexion BDD";
                        
                    } else {
                        
                        $Pseudo=htmlentities($_POST['pseudo'],ENT_QUOTES,"UTF-8");

                        $Mdp=md5($_POST['mdp']);
                        $req=mysqli_query($mysqli,"SELECT * FROM admin WHERE pseudo='$Pseudo' AND mdp='$Mdp'");
                        
                        if(mysqli_num_rows($req)!=1){
                            echo "Pseudo ou mot de passe incorrect.";
                        } else {
                            
                            $_SESSION['pseudo']=$Pseudo;
                            echo "Connexion réussie, accéder à l'accueil du site en <a href='index_bis.php'>cliquant ici</a>.";
                            $TraitementFini=true;
                        }
                    }
                }
            }
            if(!isset($TraitementFini)){
                ?>
                
                <p>Remplissez le formulaire ci-dessous pour vous connecter en tant qu'admin :</p>
                <form method="post" action="connexion_admin.php">
                    <div class="row">
                        <div class="col s4 offset-s4"> 
                    <input type="text" name="pseudo" placeholder="Votre pseudo..." required>
                    <input type="password" name="mdp" placeholder="Votre mot de passe..." required>
                </div>
            </div>

                    <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="valider" value="Connexion">
                </form>
                <br>
                <form method="post" action="index.php">
                 <input class="waves-effect waves-light btn light-blue darken-3"  type="submit" name="" value="retour menu de connexion">
                </form>

                <?php
            }
        }
        ?>
    </div>
    </body>
</html>