<nav class="blue-grey lighten-1" style="padding:0px 10px; position: fixed;">
	<div class="nav-wrapper">
		<a href="index_bis.php" class="brand-logo">APPLI-Film</a>

		<a href="#" class="sidenav-trigger" data-target="mobile-nav">
			<i class="material-icons">menu</i>
		</a>

		<ul class="right hide-on-med-and-down "  >
			 
			<li><a href="formFILM.php">Ajouter film</a></li> 
			<li><a href="formACTEUR.php">Ajouter acteur</a></li>  
			<li><a href="formREALISATEUR.php">Ajouter réalisateur</a></li> 
			<li><a href="choixmodif.php">Modifier un film</a></li> 
			<li><a href="espace_admin.php">Espace admin</a></li>
			<li><a href="deconnexion.php">Deconnexion</a></li>
		</ul>
	</div>
</nav>


<ul class="sidenav" id="mobile-nav">
	<li><a href="index_bis.php">Accueil</a></li>
	<li><a href="formFILM.php">Ajouter film</a></li> 
	<li><a href="formACTEUR.php">Ajouter acteur</a></li>  
	<li><a href="formREALISATEUR.php">Ajouter réalisateur</a></li> 
	<li><a href="choixmodif.php">Modifier un film</a></li> 
	<li><a href="espace_admin.php">Espace admin</a></li>
	<li><a href="deconnexion.php">Deconnexion</a></li>
</ul><br>







</header>
<main>