<?php include 'functions.php' ?>
<?php include 'header_utilisateur.php' ?>
<title>film</title>
<form action="ficheFilm.php" method="post">
	<div class="row">
		<div class="col s4">
	<label for="film"><h5> Choisissez un film parmis la liste</h5></label><br>
</div>
</div>
	
    <div class="row">
    	<div class="col s4">
    <select class="browser-default" name="film_id">
		<?php foreach($tableFilm as $value):?>
		<option value="<?php echo $value['film_id'];?>">
			<?php echo $value['film_titre'];?>
		</option>
		<?php endforeach;?>
	</select><br>
	</div>
</div>
<div class="row">
	<div class="col s2 offset-s1 ">
	
  	<button  class="waves-effect waves-light btn light-blue darken-3" name="form_submit" value="1" type="submit">Confirmer</button>
</div>
</div>

</form>


<div class="row">
	<div class="col s3 offset-s1 ">
<form action="touslesfilms.php">
  <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Voir tous les films</button>
</form>
<br>

<form action="index_utilisateur.php">
  <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Retour à l'accueil</button>
</form>

</div>
</div>
<?php include 'footer.php' ?>