<?php include 'functions.php'; ?>
<?php include 'header_utilisateur.php';?>
<title>Classement films</title>
<div class="row">
<div class="col s5">
<h3>Classement des films :</h3><br>
</div>
</div>
<br>

<?php
echo "<div class='row'>";
echo "<div class='col s6 offset-s1'>";
// Connexion à MySQL
$connection = mysqli_connect("localhost", "root", "", "bdd film"); 
  
if($connection === false){ 
    die("ERROR: Could not connect. "  
                . mysqli_connect_error()); 
} 
     $sql = "SELECT * FROM film ORDER BY film_note DESC"; 

if($res = mysqli_query($connection, $sql)){ 
    if(mysqli_num_rows($res) > 0){ 
        echo "<table class='striped'>"; 
            echo "<tr>"; 
            echo "<th>Titre</th>"; 
            echo "<th>Note</th>"; 
            echo "</tr>"; 
        while($row = mysqli_fetch_array($res)){ 
            echo "<tr>"; 
                echo "<td>" . $row['film_titre'] . "</td>"; 
                echo "<td>" . $row['film_note'] . "</td>"; 
                
            echo "</tr>"; 
        } 
        echo "</table>"; 
        mysqli_free_result($res); 
    } else{ 
        echo "No matching records are found."; 
    } 
} else{ 
    echo "ERROR: Could not able to execute $sql. " 
                                . mysqli_error($connection); 
} 
mysqli_close($connection); 
echo "</div>";
echo "</div>";
?>
<br>
<br>
<div class="row">
    <div class="col s3 offset-s1">
<form action="index_utilisateur.php">
  <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Retour à l'accueil</button>
</form>
</div>
</div>

<?php include 'footer.php' ?>