<?php include 'header_bis.php' ?>
<?php
    
?><!DOCTYPE HTML>
<html>
    <head>
        <title>Inscription</title>
    </head>
    <body>
        <div class="center">
        <h1>Inscription</h1>
        
               
        <?php
        
        if(isset($_POST['valider'])){
            
            if(!isset($_POST['pseudo'],$_POST['mdp'],$_POST['mail'])){
                echo "Un des champs n'est pas reconnu.";
            } else {
                
                if(!preg_match("#^[a-z0-9]{1,15}$#",$_POST['pseudo'])){
                    
                    echo "Le pseudo est incorrect, doit contenir seulement des lettres minuscules et/ou des chiffres, d'une longueur minimum de 1 caractère et de 15 maximum.";
                    
                } else {
                    
                    if(strlen($_POST['mdp'])<5 or strlen($_POST['mdp'])>15){
                        echo "Le mot de passe doit être d'une longueur minimum de 5 caractères et de 15 maximum.";
                    } else {
                       
                        if(!preg_match("#^[a-z0-9_-]+((\.[a-z0-9_-]+){1,})?@[a-z0-9_-]+((\.[a-z0-9_-]+){1,})?\.[a-z]{2,30}$#i",$_POST['mail'])){
                           
                            echo "L'adresse mail est incorrecte.";
                            
                        } else {
                            if(strlen($_POST['mail'])<7 or strlen($_POST['mail'])>50){
                                echo "Le mail doit être d'une longueur minimum de 7 caractères et de 50 maximum.";
                            } else {
                                
                                $mysqli=mysqli_connect('localhost','root','','bdd film');
                                if(!$mysqli) {
                                    echo "Erreur connexion BDD";
                                    
                                } else {
                                    $Pseudo=htmlentities($_POST['pseudo'],ENT_QUOTES,"UTF-8");//
                                    $Mdp=md5($_POST['mdp']);

                                    $Mail=htmlentities($_POST['mail'],ENT_QUOTES,"UTF-8");
                                    if(mysqli_num_rows(mysqli_query($mysqli,"SELECT * FROM membres WHERE pseudo='$Pseudo'"))!=0){
                                        echo "Ce pseudo est déjà utilisé par un autre membre, veuillez en choisir un autre svp.";
                                    } else {
                                        
                                        if(mysqli_query($mysqli,"INSERT INTO membres SET pseudo='$Pseudo', mdp='$Mdp', mail='$Mail'")){
                                            echo "Inscrit avec succès! Vous pouvez vous connecter: <a href='connexion.php'>Cliquez ici</a>.";
                                            $TraitementFini=true;
                                        } else {
                                            echo "Une erreur est survenue, merci de réessayer ou contactez-nous si le problème persiste.";
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(!isset($TraitementFini)){
            ?>
            
            <p>Remplissez le formulaire ci-dessous pour vous inscrire:</p>
            <form method="post" action="inscription.php">
                <div class="row">
                <div class="col s4 offset-s4 "><input type="text" name="pseudo" placeholder="Votre pseudo..." required>
                <input type="password" name="mdp" placeholder="Votre mot de passe..." required>
                <input type="email" name="mail" placeholder="Votre mail..." required>
            </div>
        </div>


                <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="valider" value="s'inscrire">
            
            </form>
            <br>

             <form method="post" action="index.php">
            <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="" value="Retour menu de connexion">
            </form>

            <?php
        }
        ?>
    </div>
    </body>
</html>