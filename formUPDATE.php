<?php include 'functions.php' ?>
<?php include 'header.php' ?>

<?php $film=getFilmByID(10); ?>

<form name="form1" method="post" action="update.php">
<table class="table table-responsive">
<tr>
<td>id film</td>
<td><?php  echo $film['film_id']; ?></td>
</tr>
<tr>
<td>Titre :</td>
<td><input type="text" name="film_titre" value="<?php echo htmlentities($film['film_titre']); ?>"></td>
</tr>
<tr>
<td>Genre :</td>
<td>
<input type="text" name="film_genre" value="<?php echo htmlentities($film['film_genre']); ?>">
</td>
</tr>
<tr>
<td>Date de sortie :</td>
<td>
<input type="date" name="film_date_sortie" value="<?php echo $film['film_date_sortie'];?>"/>
</td>
</tr>
<tr>
<td>Duree :</td>
<td>
<input type="text" name="film_duree" value="<?php echo htmlentities($film['film_duree']); ?>">
</td>
</tr>
<tr>
<td>Production :</td>
<td>
<input type="text" name="film_production" value="<?php echo htmlentities($film['film_production']); ?>">
</td>
</tr>
<tr>
<td>Distribution :</td>
<td>
<input type="text" name="film_distribution" value="<?php echo htmlentities($film['film_distribution']); ?>">
</td>
</tr>
<tr>
<td>Resume :</td>
<td>
<input type="text" name="film_resume" value="<?php echo htmlentities($film['film_resume']); ?>">
</td>
</tr>
<tr>
<td>Note :</td>
<td><input type="text" name="film_note" value="<?php echo htmlentities($film['film_note']); ?>"></td>
</tr>
<tr>
<td><input type="hidden" name="film_id" value="<?php echo $film['film_id']; ?>"></td>
<td>
<input type="hidden" name="MM_update" value="form1">
<input type="submit" name="Bouton" value="Modifier">
</td>
</tr>
</table>
</form>

<?php include 'footer_admin.php' ?>
