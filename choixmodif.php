<?php include 'functions.php' ?>
<?php include 'header.php' ?>
<title>Choix modification</title>
<div class="row">
	<div class="col s5">
<form action="ficheModif.php" method="post">
	<h4>Choix modification :</h4><br>
	<label for="film"><h6> Choisissez un film parmis la liste.</h6></label><br>
	<br>
        <div class="row">
    	<div class="col s4">
    <select class="browser-default" name="film_id">
		<?php foreach($tableFilm as $value):?>
		<option value="<?php echo $value['film_id'];?>">
			<?php echo $value['film_titre'];?>
		</option>
		<?php endforeach;?>
	</select><br>
	</div>
</div>	
	<br>
  	<button class="waves-effect waves-light btn light-blue darken-3" name="form_submit" value="1" type="submit">Confirmer</button>

</form>
<br>



<form action="index_bis.php">
  <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Retour à l'accueil</button>
</form>
</div>
</div>
<?php include 'footer.php' ?>