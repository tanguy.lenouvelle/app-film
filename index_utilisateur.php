<?php include 'functions.php'; ?>

<?php include 'header_utilisateur.php';?>
    <title>APPLI-Film</title>
<br>
<br>
<br>
 	<div class="row" class="center-align">

      <div class="center col s4">       
         <div class="row center-align">
   			 <div class="col s12 m4 l2 "></div>
		   		<div class="col s12 m4 l8 ">
    				<a class="light-blue-text text-darken-3" href="parcourir.php"><i class="large material-icons">movie</i></a>
    				<div class="section">
    					<h5>Film</h5>
    					<p><center>Cliquez sur l'icon ci-dessus pour accèder à la fiche de l'acteur de votre choix.</center></p></div></div>
   							 <div class="col s12 m4 l2 "></div>
		</div>
        </div>

      <div class="center col s4">
      	<div class="row center-align">
    		<div class="col s12 m4 l2 "></div>
    			<div class="col s12 m4 l8 ">
    				<a class="light-blue-text text-darken-3" href="parcourirActeur.php"><i class="large material-icons ">people</i></a>
    				<div class="section">
    					<h5>Acteur</h5>
    					<p><center>Cliquez sur l'icon ci-dessus pour accèder à la fiche de l'acteur de votre choix.</center></p></div></div>
    						<div class="col s12 m4 l2 "></div>
		</div>
        </div>

      <div class="center col s4 ">
     	 <div class="row center-align">
    	 	<div class="col s12 m4 l2 "></div>
    			<div class="col s12 m4 l8">    				
       					<a class="light-blue-text text-darken-3" href="parcourirRea.php"><i class="large material-icons">videocam</i></a>
       					
 						 <div class="section">
       						<h5>Réalisateur</h5>		
       						<p><center>Cliquez sur l'icon ci-dessus pour accèder à la fiche du réalisateur de votre choix.</center></p></div>
       					</div>
    							<div class="col s12 m4 l2 "></div>
    		   </div>
         </div>
    </div>
          

<?php include 'footer.php';?>