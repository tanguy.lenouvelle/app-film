<nav class="blue-grey lighten-1" style="padding:0px 10px; position: fixed;">
	<div class="nav-wrapper">
		<a href="index_utilisateur.php" class="brand-logo">APPLI-Film</a>

		<a href="#" class="sidenav-trigger" data-target="mobile-nav">
			<i class="material-icons">menu</i>
		</a>

		<ul class="right hide-on-med-and-down "  >
			<li><a href="parcourir.php">Parcourir la liste des films</a></li> 
			<li><a href="parcourirActeur.php">Parcourir la liste des acteurs</a></li>
			<li><a href="parcourirRea.php">Parcourir la liste des réalisateurs</a></li> 
			<li><a href="note.php">Classement des films</a></li> 
			<li><a href="espace-membre.php">Espace membre</a></li> 
			<li><a href="deconnexion.php">Deconnexion</a></li>
		</ul>
	</div>
</nav>


<ul class="sidenav" id="mobile-nav">
	<li><a href="index_utilisateur.php">Accueil</a></li>
	<li><a href="parcourir.php">Parcourir la liste des films</a></li> 
	<li><a href="parcourirActeur.php">Parcourir la liste des acteurs</a></li>
	<li><a href="parcourirRea.php">Parcourir la liste des réalisateurs</a></li> 
	<li><a href="note.php">Classement des films</a></li> 
	<li><a href="espace-membre.php">Espace membre</a></li> 
	<li><a href="deconnexion.php">Deconnexion</a></li>
</ul><br>







</header>
<main>

