<?php

$filmId = $_POST['film_id'];
$form_submit = $_POST['form_submit']

?>

<?php include 'functions.php'; ?>
<?php include 'header.php';?>

  	
    <?php foreach($tableFilm as $value):?>
        <?php if ($form_submit == '1')?>
        <?php if($filmId == $value['film_id']):?>
<div class="row">
    <div class="col s6">
        <form name="form1" method="post" action="update.php">
        <table class="table table-responsive">
        <tr>
        <td>ID film :</td>
        <td><?php  echo $value['film_id']; ?></td>
        </tr>
        <tr>
        <td>Titre :</td>
        <td><input type="text" name="film_titre" value="<?php echo htmlentities($value['film_titre']); ?>"></td>
        </tr>
        <tr>
        <td>Genre :</td>
        <td>
        <input type="text" name="film_genre" value="<?php echo htmlentities($value['film_genre']); ?>">
        </td>
        </tr>
        <tr>
        <td>Date de sortie :</td>
        <td>
        <input type="date" name="film_date_sortie" value="<?php echo $value['film_date_sortie'];?>"/>
        </td>
        </tr>
        <tr>
        <td>Duree :</td>
        <td>
        <input type="text" name="film_duree" value="<?php echo htmlentities($value['film_duree']); ?>">
        </td>
        </tr>
        <tr>
        <td>Production :</td>
        <td>
        <input type="text" name="film_production" value="<?php echo htmlentities($value['film_production']); ?>">
        </td>
        </tr>
        <tr>
        <td>Distribution :</td>
        <td>
        <input type="text" name="film_distribution" value="<?php echo htmlentities($value['film_distribution']); ?>">
        </td>
        </tr>
        <tr>
        <td>Resume :</td>
        <td>
        <input type="text" name="film_resume" value="<?php echo htmlentities($value['film_resume']); ?>">
        </td>
        </tr>
        <tr>
        <td>Note :</td>
        <td><input type="text" name="film_note" value="<?php echo htmlentities($value['film_note']); ?>"></td>
        </tr>
        <tr>
        <input type="hidden" name="film_id" value="<?php echo $value['film_id']; ?>">
</div>
</div>
        </tr>
        </table>
        <br>
        <div class="row">
            <div class="col s3 offset-s1 ">
        <input type="hidden" name="MM_update" value="form1">
        <input class="waves-effect waves-light btn light-blue darken-3" type="submit" name="Bouton" value="Modifier">
        </form>
            </div>
        </div>

      <?php endif; ?>
      <?php endforeach;?>

    <br>

    <div class="row">
        <div class="col s5 ">
            <form action="index_bis.php">
                <button class="waves-effect waves-light btn light-blue darken-3" type="submit">Retour à l'accueil</button>
            </form>
        </div>
    </div>
    
<?php include 'footer_admin.php' ?>