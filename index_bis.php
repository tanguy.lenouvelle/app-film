<?php include 'functions.php'; ?>
<?php include 'header.php';?>
    <title>APPLI-Film</title>

<br>
<br>
<br>
 	<div class="row" class="center-align">

      <div class="center col s4">       
         <div class="row center-align">
   			 <div class="col s12 m4 l2 "></div>
		   		<div class="col s12 m4 l8 ">
    				<a class="light-blue-text text-darken-3" href="formFILM.php"><i class="large material-icons">movie</i></a>
    				<div class="section">
    					<h5>Film</h5>
    					<p><center>Cliquez sur l'icon ci-dessus pour ajouter un film à la base de données</center></p></div></div>
   							 <div class="col s12 m4 l2 "></div>
		</div>
        </div>

      <div class="center col s4">
      	<div class="row center-align">
    		<div class="col s12 m4 l2 "></div>
    			<div class="col s12 m4 l8 ">
    				<a class="light-blue-text text-darken-3" href="formACTEUR.php"><i class="large material-icons ">people</i></a>
    				<div class="section">
    					<h5>Acteur</h5>
    					<p><center>Cliquez sur l'icon ci-dessus pour ajouter un acteur à la base de données</center></p></div></div>
    						<div class="col s12 m4 l2 "></div>
		</div>
        </div>

      <div class="center col s4 ">
     	 <div class="row center-align">
    	 	<div class="col s12 m4 l2 "></div>
    			<div class="col s12 m4 l8">    				
       					<a class="light-blue-text text-darken-3" href="formREALISATEUR.php"><i class="large material-icons">videocam</i></a>
       					
 						 <div class="section">
       						<h5>Réalisateur</h5>		
       						<p><center>Cliquez sur l'icon ci-dessus pour ajouter un réalisateur à la base de données</center></p></div>
       					</div>
    							<div class="col s12 m4 l2 "></div>
    		   </div>
         </div>
    </div>
          

<?php include 'footer_admin.php';?>